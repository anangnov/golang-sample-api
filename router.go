package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/anangnov/golang-sample-api/controllers"
)

func Router() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Ping",
		})
	})
	r.POST("/create", controllers.CreateData)
	r.GET("/list", controllers.ListData)

	return r
}
