# golang-sample-api

# FEATURES
1. gin-gonic
3. gorm
2. mySQL


# INITIAL STEP
1. Clone
2. Duplicate .env.example -> .env
3. Modify .env
4. Install golang modules
```
$ go mod download
```
5. RUN app
```
$ go run .
```
6. Check with open in the browser http://host:port
7. Sample test hit api
```
[POST] localhost:3030/create
{
    "email": "anangnov99@gmail.com",
    "address": "mojokerto",
    "phone": "08512273483"
}


[GET] localhost:3030/list
```
