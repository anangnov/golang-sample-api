package users

type CreateValidation struct {
	Email   string `json:"email" form:"email" binding:"required"`
	Address string `json:"address" form:"address" binding:"required"`
	Phone   string `json:"phone" form:"phone" binding:"required"`
}

func NewCreateValidation() CreateValidation {
	return CreateValidation{}
}
