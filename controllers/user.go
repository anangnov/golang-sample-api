package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/anangnov/golang-sample-api/models"
	"gitlab.com/anangnov/golang-sample-api/models/users"
)

type Response struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

func CreateData(c *gin.Context) {
	createValidator := users.NewCreateValidation()
	err := c.BindJSON(&createValidator)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": "Invalid Form",
		})
		return
	}

	data := models.Users{Email: createValidator.Email, Address: createValidator.Address, Phone: createValidator.Phone}
	models.DB.Create(&data)

	c.JSON(http.StatusOK, gin.H{
		"message": "Created",
		"data":    data,
	})
}

func ListData(c *gin.Context) {
	data := []models.Users{}
	models.DB.Find(&data)

	c.JSON(http.StatusOK, gin.H{
		"message": "Found",
		"data":    data,
	})
}
